package com.malencur.communication.socket

/**
 *
 * Defines basic steps to be made during validation
 * process between two TCP/IP sockets.
 *
 * @since 1.0.0
 * @author alog
 */
interface CommunicationNodeValidatorListener<in B> {
    /**
     * Notifies that communication validation has been done successfully.
     * @param bean A Bean that holds specific for each project data to be used after validation process.
     */
    fun onValidate(bean: B?)
}