package com.malencur.communication.socket

import org.slf4j.LoggerFactory
import java.io.EOFException
import java.io.IOException
import java.io.ObjectInputStream
import java.net.Socket
import java.nio.channels.Channels
import java.nio.channels.ClosedByInterruptException
import java.util.*

/**
 * Uses socket to listen for input messages
 *
 * @property socket TCP socket ready to receive messages
 *
 * @since 1.0.0
 * @author alog
 */
class InPut(private val socket: Socket) : Thread() {
    @Volatile
    private var myThread: Thread? = null
    private var input: ObjectInputStream? = null
    private var listeners: MutableList<InputListener>

    init {
        this.name = THREAD_NAME
        myThread = this
        listeners = ArrayList()
        this.input = null
    }

    fun stopThread() {
        val tmpThread = myThread
        myThread = null
        tmpThread?.interrupt()
    }

    override fun run() {
        if (myThread == null) {
            return  // stopped before started.
        }
        try {
            val channel = Channels.newChannel(socket.inputStream)
            input = ObjectInputStream(Channels.newInputStream(channel))
            while (true) {
                //get object from server, will block until object arrives.
                val messageObject = input!!.readObject()
                for (l in listeners) {
                    l.onMessage(messageObject)
                }

                Thread.yield() // let another thread have some time perhaps to stop this one.
                if (Thread.currentThread().isInterrupted) {
                    if (input != null)
                        try {
                            input!!.close()
                        } catch (ex: IOException) {
                            log.error(ex.message, ex.cause)
                        }

                    throw InterruptedException("Thread has been interrupted")
                }
            }
        } catch (ex: Exception) {

            when (ex) {
                is EOFException -> log.debug("socket disconnected")
                is InterruptedException, is ClosedByInterruptException -> log.debug("thread has been interrupted")
                else -> log.error(ex.message, ex.cause)
            }

            if (input != null)
                try {
                    input!!.close()
                } catch (e: IOException) {
                    log.error(ex.message, ex.cause)
                }

            for (l in listeners) {
                l.onClose(socket)
            }
        }

    }

    fun addInputListener(listener: InputListener) {
        listeners.add(listener)
    }

    interface InputListener {
        fun onMessage(messageObject: Any)
        fun onClose(socket: Socket)
    }

    companion object {

        private val log = LoggerFactory.getLogger(InPut::class.java)

        const val THREAD_NAME = "SocketInPut"
    }


}