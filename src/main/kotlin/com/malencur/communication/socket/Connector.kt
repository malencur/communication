package com.malencur.communication.socket

import org.slf4j.LoggerFactory
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

@Suppress("unused", "MemberVisibilityCanBePrivate")
/**
 *
 * Provides an app with a [Client] object to be able to talk to HostServer
 *
 * @property type The type of a client that wants to be connected to the server. Defined by project that uses this class.
 * @property id The clients id
 * @property IP The server IP address
 * @property PORT The server PORT
 *
 * @since 1.0.0
 * @author alog
 */
class Connector(
        private var type: Int,
        private var id: Int,
        private var IP: String = "localhost",
        private var PORT: Int = 1337
) {

    private val tag: String = this.javaClass.simpleName

    internal var listener: ConnectorListener? = null


    /**
     * The quantity of restart attempts after socket closed event.
     */
    private var restartsNumber = 0

    private val executorClientStarter: ExecutorService = Executors.newSingleThreadExecutor()
    private var futureClientStarter: Future<Boolean>? = null


    fun startConnection() {
        stopConnection()
        Client.id = id
        Client.hostName = IP
        Client.port = PORT
        Client.type = type
        futureClientStarter = executorClientStarter.submit(ClientStarter())
    }

    fun stopConnection() {
        if (futureClientStarter != null) futureClientStarter!!.cancel(true)
    }

    fun shutDown() {
        listener = null
        if (futureClientStarter != null) futureClientStarter!!.cancel(true)
        executorClientStarter.shutdownNow()
    }

    private fun restartClient() {
        stopConnection()
        startConnection()
    }

    fun addConnectorListener(listener: ConnectorListener) {
        this.listener = listener
    }

    interface ConnectorListener {
        fun onClientConnected(serverClient: Client)
    }

    private inner class ClientStarter : Callable<Boolean> {

        @Throws(Exception::class)
        override fun call(): Boolean? {
            var bean: ClientBean?
            // let another thread have some time perhaps to stop this one:
            Thread.yield()
            if (Thread.currentThread().isInterrupted) {
                return false
            }
            restartsNumber++
            log.info("$tag.ClientStarter.call: $restartsNumber Attempt to get connected to the Server")
            bean = Client.startInTheSameThread()
            while (null == bean) {
                // let another thread have some time perhaps to stop this one:
                Thread.yield()
                if (Thread.currentThread().isInterrupted) {
                    return false
                }
                try {
                    Thread.sleep(delay.toLong())
                } catch (ex: InterruptedException) {
                    log.error(ex.message, ex.cause)
                }

                // let another thread have some time perhaps to stop this one:
                Thread.yield()
                if (Thread.currentThread().isInterrupted) {
                    return false
                }
                restartsNumber++
                log.info("$tag.ClientStarter.call: $restartsNumber Attempt to get connected to the Server")
                bean = Client.startInTheSameThread()
            }
            listener?.onClientConnected(Client)
            return true
        }
    }

    companion object {

        private val log = LoggerFactory.getLogger(Connector::class.java)

        /**
         * An instance of this class. Realizes singleton pattern.
         */
        private var connector: Connector? = null
        /**
         * delay between two separate attempts to to obtain [Client] object
         */
        private const val delay = 2000

        /**
         * This method must be used in **production**.
         * Realizes singleton pattern.
         * @param type The type of a client that wants to be connected to the server
         * @param id The clients id
         * @param ip The server IP
         * @param port The server PORT
         *
         * @return A new instance of the class. Realizes singleton pattern.
         */
        fun getInstance(type: Int, id: Int, ip: String, port: Int): Connector {
            if (connector == null) {
                connector = Connector(type, id, ip, port)
            }
            return connector as Connector
        }

        /**
         * This method is just for **testing**. Here we use predefined IP an PORT
         * @param type The type of a client that wants to be connected to the server
         * @param id The clients id
         *
         * @return A new instance of the class. Realizes singleton pattern.
         */
        fun getInstance(type: Int, id: Int): Connector {
            if (connector == null) {
                connector = Connector(type, id, "localhost", 1337)
            }
            return connector as Connector
        }
    }
}