@file:Suppress("unused")

package com.malencur.communication.socket

import org.slf4j.LoggerFactory
import java.net.ServerSocket
import java.net.Socket

/**
 *
 * Provides basic server functionality.
 *
 * It's generalized by a predefined object that must
 * implement [SocketManager] interface.
 *
 * @property generalizedObject An object that implements [SocketManager] interface
 * @property PORT The port this server will be listening to.
 *
 * @since 1.0.0
 * @author alog
 */
class Server<E : SocketManager>(var generalizedObject: E, private var PORT: Int = 1337) {

    private var serverAcceptor: ServerAcceptor? = null

    /**
     * Accepts incoming client connections
     */
    inner class ServerAcceptor internal constructor(private var serverSocket: ServerSocket) : Thread() {
        @Volatile
        private var myThread: Thread? = null

        init {
            this.name = "ServerAcceptor"
            myThread = this
        }

        fun stopThread() {
            val tmpThread = myThread
            myThread = null
            tmpThread?.interrupt()
        }

        override fun run() {

            if (myThread == null) {
                log.info("server: stopped before running")
                return  // stopped before started.
            }

            try {
                log.info("server: Running on port ${serverSocket.localPort}")
                while (true) {
                    val socket: Socket = serverSocket.accept()
                    // this blocks, waiting for a Socket to the client
                    log.info("server: got client")
                    generalizedObject.accept(socket)
                    Thread.yield() // let another thread have some time perhaps to stop this one.
                    if (Thread.currentThread().isInterrupted) {
                        log.info("server: stopped")
                    }
                }
            } catch (ex: Exception) {
                log.error(ex.message, ex.cause)
            }

        }
    }

    /**
     * Runs the [ServerAcceptor] to catch incoming client connections
     */
    fun start(): Boolean {
        return try {
            serverAcceptor = ServerAcceptor(ServerSocket(PORT))
            serverAcceptor!!.start()
            true
        } catch (ex: Exception) {
            false
        }
    }

    /**
     * Stops the server.
     */
    fun stop() {
        generalizedObject.closeAll()
        serverAcceptor?.stopThread()
    }

    companion object {
        private val log = LoggerFactory.getLogger(Server::class.java)
    }
}