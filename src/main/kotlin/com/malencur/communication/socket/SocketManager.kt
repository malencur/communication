package com.malencur.communication.socket

import java.net.Socket

/**
 * This interface must be implemented by any class that manages
 * sockets received from Server.ServerAcceptor
 *
 * @since 1.0.0
 * @author alog
 */
interface SocketManager {
    /**
     * Implement this method to use the given socket.
     * @param socket A socket to be accepted by the server
     */
    fun accept(socket: Socket)

    /**
     * Implement this method to close all active sockets.
     */
    fun closeAll()
}