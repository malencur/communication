package com.malencur.communication.socket

/**
 *
 * Defines basic communication functionality between two TCP/IP sockets.
 *
 * @since 1.0.0
 * @author alog
 */
interface CommunicationNodeListener {
    /**
     * Notifies that this socket has received a message from another socket.
     * @param obj A message from the server. Before use, this message must be cast to a project specific message type.
     */
    fun onInputMessage(obj: Any)

    /**
     * Notifies that communication with a remote socket has been broken.
     */
    fun onCloseSocket()
}